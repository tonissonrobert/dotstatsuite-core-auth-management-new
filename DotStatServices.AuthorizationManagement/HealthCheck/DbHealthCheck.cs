﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace DotStatServices.AuthorizationManagement.HealthCheck
{
    [ExcludeFromCodeCoverage]
    public class DbHealthCheck : IHealthCheck
    {
        private readonly IGeneralConfiguration _configuration;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public DbHealthCheck(IGeneralConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            var databaseVersion = await CheckSqlServerConnection(_configuration.DotStatSuiteCoreCommonDbConnectionString, cancellationToken);

            var result = !string.IsNullOrEmpty(databaseVersion);

            var data = new Dictionary<string, object>()
            {
                {"databaseAlive", result},
                {"databaseVersion", databaseVersion}
            };

            return result 
                ? HealthCheckResult.Healthy(data:data) 
                : HealthCheckResult.Unhealthy(data:data);
        }

        private async Task<string> CheckSqlServerConnection(string connectionString, CancellationToken cancellationToken)
        {
            try
            {
                await using var connection = new SqlConnection(connectionString);

                await connection.OpenAsync(cancellationToken);

                await using var command = connection.CreateCommand();

                command.CommandText = "select [Version] from [dbo].[DB_VERSION]";

                return await command.ExecuteScalarAsync(cancellationToken) as string;
            }
            catch
            {
                return null;
            }
        }
    }
}
